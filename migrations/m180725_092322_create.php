<?php

use yii\db\Migration;

/**
 * Class m180725_092322_create
 */
class m180725_092322_create extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('paid', [
            'status' => 'Awiting Payment',
        ]);

                $this->insert('paid', [
            'status' => 'Paid',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180725_092322_create cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180725_092322_create cannot be reverted.\n";

        return false;
    }
    */
}
