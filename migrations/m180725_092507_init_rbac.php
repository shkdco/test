<?php

use yii\db\Migration;

/**
 * Class m180725_092507_init_rbac
 */
class m180725_092507_init_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

            $auth = Yii::$app->authManager;
            
            // add "author" role and give this role the "createPost" permission
            $vaad = $auth->createRole('vaad');
            $auth->add($vaad);
    
            $resident = $auth->createRole('resident');
            $auth->add($resident);
    
            
            $auth->addChild($vaad, $resident);
            
            $updateDeleteUsers = $auth->createPermission('updateDeleteUsers');
            $auth->add($updateDeleteUsers);
    
            $createPayment = $auth->createPermission('createPayment');
            $auth->add($createPayment);

            $viewUser = $auth->createPermission('viewUser');
            $auth->add($viewUser);

            $indexUser = $auth->createPermission('indexUser');
            $auth->add($indexUser);

            $deleteUser = $auth->createPermission('deleteUser');
            $auth->add($deleteUser);

            $DeletePayment = $auth->createPermission('DeletePayment');
            $auth->add($DeletePayment); 

            $viewPayment = $auth->createPermission('viewPayment');
            $auth->add($viewPayment);

            $indexPayment = $auth->createPermission('indexPayment');
            $auth->add($indexPayment);
            
            $updatePayment = $auth->createPermission('updatePayment');
            $auth->add($updatePayment);

            $updateOwnPayment = $auth->createPermission('updateOwnPayment');
    
            $rule = new \app\rbac\RRule;
            $auth->add($rule);
            
            $updateOwnPayment->ruleName = $rule->name;                 
            $auth->add($updateOwnPayment);                                                    
            
            $auth->addChild($vaad, $updateDeleteUsers);
            $auth->addChild($vaad, $DeletePayment);
            $auth->addChild($resident, $createPayment);
            $auth->addChild($resident, $viewPayment);
            $auth->addChild($resident, $indexPayment);
            $auth->addChild($resident, $indexUser);
            $auth->addChild($resident, $viewUser);
            $auth->addChild($resident, $updateOwnPayment); 
            $auth->addChild($updateOwnPayment, $updatePayment);     
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180725_092507_init_rbac cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180725_092507_init_rbac cannot be reverted.\n";

        return false;
    }
    */
}
