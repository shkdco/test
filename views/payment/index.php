<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Payment;
use app\models\User;
use app\models\Paid;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PaymentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Payments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Payment', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'sum',         
            [
                'label'=>'Paid',
            'value' =>  function ($data) {
                return Paid::findOne(['id'=>(($data->paid)+1)])->status;
            },
            ],

            ['label'=>'Resident',
            'value' => function ($data) {
                return User::findOne(['id'=>$data->resident])->name;
            },],

            'date',
            //'created_at',
            //'updated_at',
            //'created_by',
            [
                'label' => 'Created By',
                'value' => function ($data) {
                return User::findOne(['id'=>$data->created_by])->name;
            },
            ], 
            //'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
