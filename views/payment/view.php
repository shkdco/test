<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Payment;
use app\models\User;
use app\models\Paid;

/* @var $this yii\web\View */
/* @var $model app\models\Payment */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Payments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'sum',
           [  'label'=>'Paid',
            'value' =>  function ($data) {
                return Paid::findOne(['id'=>(($data->paid)+1)])->status;
            },
            ],


          [                      
                'label' => 'resident',
				'format' => 'html',
				'value' => Html::a($model->resident1->name, 
					['user/view', 'id' => $model->resident1->id]),                
            ],

            
            'date',
            'created_at',
            'updated_at',
            
            [
                'label' => 'Created By',
                'value' => function ($data) {
                return User::findOne(['id'=>$data->created_by])->name;
            },
            ], 
            
            [
                'label' => 'Updated By',
                'value' => function ($data) {
                return User::findOne(['id'=>$data->updated_by])->name;
            },
            ], 
        ],
    ]) ?>

</div>
